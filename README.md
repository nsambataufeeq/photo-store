# Photo Store

## Brief
The photo store app is a project to store photos clerks of works under sigma.

## Goals
Through remote photo storage, the project aims to alleviate needless local storage requirements as well as data loss.
Archiving project photos will help in future project planning.

### Where we are.
Project Phase Alpha: Design adn development of a web platform, primarily focused on mobile.

### Where we hope to be:
Have a mobile native app as well as Desktop native.
