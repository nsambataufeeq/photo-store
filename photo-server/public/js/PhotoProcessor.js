<script src="https://cdnjs.cloudflare.com/ajax/libs/jimp/0.16.1/jimp.min.js" integrity="sha512-CwJxtyYUXii1/F1YLND+mdqhu6ffU3MazNXwYnN3uJrCyt7yR2S24r4JePCday1oz8vSEJBJ9ofzJsKPDqSAjg==" crossorigin="anonymous"></script>
onmessage = function(e) {

    Jimp.read(e.data.aBuf).then((photo)=>{
        photo.resize(1024,1024).getBase64(e.data.mime).then(res=>{
            const data = {url:res,name:e.data.name};
            postMessage(res);
        });
    });
}
