<?php
namespace App\Traits;
use App\Models\Photo;
use Illuminate\Support\Collection;
use Ramsey\Uuid\Uuid;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

trait RBlockTrait{
    public Collection $photoz;
    public function savePhotos(int $sblockId){
        $this->photoz->map(function($photo) use ($sblockId){
            $photo = (object)$photo;
            $tmp = new Photo();
            $tmp->siteblock_id = $sblockId;
            $fparts = explode('.',$photo->name);
            $extension = $fparts[count($fparts)-1];
            $url = Uuid::uuid4().'.'.$extension;
            list($type, $file_data) = explode(';', $photo->uri);
            list(, $file_data) = explode(',', $file_data);
            $img = Image::make($file_data);
            Storage::put('photos/'.$url,base64_decode($file_data));
            $img->resize(400,400, function($constraint) {
                $constraint->aspectRatio();
            })->orientate()->encode($extension,80);
            Storage::disk('public')->put($url,$img);
            $tmp->caption= $photo->caption;
            $tmp->url = $url;
            $tmp->created_at = $photo->cdate;
            $tmp->save(['timestamps'=>false]);
        });
    }
}
