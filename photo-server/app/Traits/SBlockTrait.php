<?php
namespace App\Traits;
use Illuminate\Support\Facades\Auth;
use App\Models\Site;
trait SBlockTrait{
    public function owner(int $siteId){
        $site = Site::find($siteId);
        $user = $site->user;
        $currentUser = Auth::user();
        $owned = $user->id==$currentUser->id;
        return response($owned,200);
    }
}
