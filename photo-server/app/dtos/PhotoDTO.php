<?php
namespace App\dtos;
use App\Models\Photo;
class PhotoDTO{
    public string $url;
    public string $caption;
    public function __construct(Photo $photo)
    {
       $this->url = $photo->url;
       $this->caption = $photo->caption;
    }
}
