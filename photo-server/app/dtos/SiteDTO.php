<?php
namespace App\dtos;
use Illuminate\Support\Carbon;
class SiteDTO{
    public int $id;
    public string $name;
    public string $lastPhoto;
    public string $elapsedTime;
    public function __construct(int $id,string $name,string $lastPhoto,string $cdate)
    {
        $this->id = $id;
        $this->name = $name;
        $this->lastPhoto = $lastPhoto;
        $this->elapsedTime = Carbon::parse($cdate)->diffForHumans();
    }
}
