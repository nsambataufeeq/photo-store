<?php
namespace App\dtos;

use Illuminate\Database\Eloquent\Collection;
class SiteBlockDTO{
    public int $id;
    public string $name;
    public array $photos;
    public int $pcount;
    public function __construct(int $id,string $name,Collection $photos)
    {
        $this->id = $id;
        $this->name = $name;
        $this->photos = $photos->sortByDesc('created_at')->take(5)->pluck('url')->all();
        $this->pcount = $photos->count();
    }
}
