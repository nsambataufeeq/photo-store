<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Photo;
use App\Models\SiteBlock;
class Block extends Model
{
    //
    protected $table="blocks";
    public function photos(){
        return $this->hasManyThrough(Photo::class,SiteBlock::class,'block_id','siteblock_id');
    }
}
