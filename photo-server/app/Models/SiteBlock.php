<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Photo;
use App\Models\Block;
use App\Models\Site;
class SiteBlock extends Model
{
    //
    protected $table = "siteblocks";
    public function photos(){
        return $this->hasMany(Photo::class,'siteblock_id');
    }
    public function block(){
        return $this->belongsTo(Block::class,'block_id');
    }
    public function site(){
        return $this->belongsTo(Site::class,'site_id');
    }
}
