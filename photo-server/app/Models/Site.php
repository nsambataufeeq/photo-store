<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\SiteBlock;
use App\Models\Block;
class Site extends Model
{
    //
    protected $table = 'sites';
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function siteblocks(){
        return $this->hasMany(SiteBlock::class,'site_id');
    }
    public function blocks(){
        return $this->belongsToMany(Block::class, 'siteblocks', 'site_id', 'block_id');
    }
}
