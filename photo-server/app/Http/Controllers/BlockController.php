<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use App\Models\SiteBlock;
use App\dtos\BlockDTO;
use App\dtos\PhotoDTO;
use App\Traits\RBlockTrait;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class BlockController extends Controller
{
    //
    use RBlockTrait;
    public function __construct()
    {
        $this->middleware("auth:api");
    }
    public function index(int $blockId){
        $sblock = SiteBlock::find($blockId);
        $block = $sblock->block;
        $tmp = new BlockDTO($sblock->id,$block->name);
        return response()->json($tmp);
    }
    public function photos(int $blockId){
        $sblock = SiteBlock::find($blockId);
        $photos = $sblock->photos->sortByDesc("created_at")->map(function($ph){
            return new PhotoDTO($ph);
        });
        return response()->json($photos);
    }
    private function ownerUtil(int $sblockId){
        $sblock = SiteBlock::find($sblockId);
        $user = $sblock->site->user;
        $currentUser = Auth::user();
        return $user->id==$currentUser->id;
    }
    public function owner(int $sblockId){
        $ownership = $this->ownerUtil($sblockId);
        return response($ownership,200);
    }
    public function saveImages(Request $request,int $sblockId){
        $this->photoz = new Collection($request->json()->all()['photos']);
        $ownership = $this->ownerUtil($sblockId);
        if(!$ownership){
            return response(["error"=>"Forbidden"],403);
        }
        try{
            DB::beginTransaction();
            $this->savePhotos($sblockId);
            DB::commit();
            return response()->json(["status"=>"success"]);
        }catch(Exception $e){
            DB::rollBack();
            Log::info($e);
            return response()->json(['error'=>$e->getMessage()],500);
        }

    }
}
