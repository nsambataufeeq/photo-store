<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Login extends Controller
{
    //
    public function signIn(Request $request){
        $this->validate($request, [
            'phoneNo' => 'required|string|min:10',
            'password' => 'required|string|min:8']);
        $credentials = $request->only(['phoneNo','password']);
        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }
        return response()->json([
            "token" => $token,
            "expires_at" => Auth::factory()->getTTL() * 60 * 2
        ]);
    }
}
