<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\dtos\SiteDTO;
use Illuminate\Support\Facades\Auth;

class SitesController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index(){
        $subq = DB::table('photos')
                ->select(DB::raw('siteblock_id,url,MAX(photos.created_at) as pdate'))
                ->groupBy('siteblock_id');
        $subq1 = DB::table('siteblocks')
                ->select(DB::raw('site_id,sbq.url as url,MAX(sbq.pdate) as cdate'))
                ->joinSub($subq,'sbq',function($join){
                  $join->on('sbq.siteblock_id','=','siteblocks.id');
                })->groupBy('site_id');
        $result = DB::table('sites')
                ->select(DB::raw("sites.id,sites.name,sbq1.url,sbq1.cdate"))
                ->joinSub($subq1,'sbq1',function($join){
                    $join->on('sbq1.site_id','=','sites.id');
                })->orderByDesc('sbq1.cdate')->get();
        $sites = $result->map(function($el){
            $site = new SiteDTO($el->id,$el->name,$el->url,$el->cdate);
            return $site;
        });
        return response()->json($sites);
    }
    public function logout(){
        Auth::logout();
        return response("Bye, Thanks for visiting",200);
    }
}
