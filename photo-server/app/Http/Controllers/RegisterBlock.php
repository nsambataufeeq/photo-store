<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Site;
use App\dtos\BlockDTO;
use App\Models\SiteBlock;
use App\Traits\RBlockTrait;
use App\Traits\SBlockTrait;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class RegisterBlock extends Controller
{
    //
    use RBlockTrait;
    use SBlockTrait;
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function sblocks(int $siteId){
        $site = Site::find($siteId);
        $blockIds = $site->blocks->map(function($block){
            return $block->id;
        })->toArray();
        $blocks = Block::all();
        $iblocks = $blocks->filter(function($block) use ($blockIds){
            return !in_array($block->id,$blockIds);
        })->map(function($block){
            $tmp = new BlockDTO($block->id,$block->name);
            return $tmp;
        });
        return response()->json($iblocks);
    }
    public function saveImages(Request $request,int $siteId){
        $data = $request->json()->all();
        $this->photoz = new Collection($data['photos']);
        $blockId = $data['blockId'];
        try{
            DB::beginTransaction();
            $sblock = new SiteBlock();
            $sblock->block_id = $blockId;
            $sblock->site_id = $siteId;
            $sblock->save();
            $this->savePhotos($sblock->id);
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return response()->json(["error"=>$e->getMessage()]);
        }
    }
}
