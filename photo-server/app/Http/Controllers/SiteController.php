<?php

namespace App\Http\Controllers;

use App\Models\Site;
use App\dtos\SiteDTO;
use App\dtos\SiteBlockDTO;
use App\Traits\SBlockTrait;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    //
    use SBlockTrait;
    public function __construct()
    {
        $this->middleware("auth:api");
    }
    public function index(int $siteId){
        $site = Site::find($siteId);
        $siteblock = $site->siteblocks->first();
        $photo = $siteblock->photos->first();
        $tmp = new SiteDTO($site->id,$site->name,$photo->url,$site->created_at);
        return response()->json($tmp);
    }
    public function blocks(int $siteId){
        $site = Site::find($siteId);
        $blocks = $site->siteblocks->map(function($sblock){
            $block = $sblock->block;
            return new SiteBlockDTO($sblock->id,$block->name,$sblock->photos);
        });
        return response()->json($blocks);
    }
}
