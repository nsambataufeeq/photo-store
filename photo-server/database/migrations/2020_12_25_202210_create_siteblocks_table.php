<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteblocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siteblocks', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('site_id')->unsigned();
            $table->smallInteger('block_id')->unsigned();
            $table->json('photos');
            $table->timestamps();
            $table->foreign('site_id')
                ->references('id')->on('sites')
                ->onDelete('cascade');
            $table->foreign('block_id')
                ->references('id')->on('blocks')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siteblocks');
    }
}
