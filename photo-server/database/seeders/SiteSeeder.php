<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sites')->insert(array(
            [
                "name"=>"Kween Site",
                "user_id"=>1,
                "created_at"=>Carbon::now(),
                "updated_at"=>Carbon::now()
            ],
            [
                "name"=>"Lira Site",
                "user_id"=>2,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ],
            [
                "name"=>"Masaka Site",
                "user_id"=>3,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ]
        ));
    }
}
