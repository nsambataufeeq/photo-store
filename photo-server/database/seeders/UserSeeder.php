<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert(array(
            [
                'name' => 'Kidima Stephen',
                'phoneNo' => '0782666666',
                'password' => Hash::make("ROPE-FRUIT-QUEEN-7"),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name' => 'Tucson Kibalama',
                'phoneNo' => '0772666666',
                'password' => Hash::make("6-rope-usa-golf"),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name' => 'Nsamba Taufeeq',
                'phoneNo' => '0751999999',
                'password' => Hash::make("CUP-IN-HAND-7"),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name'=>'Tom Ssekitto',
                'phoneNo'=>'0752999999',
                'password'=>Hash::make("HIGH-PERFORMANCE-NETWORK-8"),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ]
        ));
    }
}
