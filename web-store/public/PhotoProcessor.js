importScripts(["https://cdnjs.cloudflare.com/ajax/libs/jimp/0.16.1/jimp.js"])
onmessage = function(e) {
    const maxSize = 0.5*Math.pow(10,6);
    let fileSize = e.data.aBuf.byteLength;
    let scaleFactor = 1;
    if(fileSize>maxSize){
        scaleFactor = maxSize/fileSize;
    }
    Jimp.read(e.data.aBuf).then((photo)=>{
        photo.scale(scaleFactor).getBase64(Jimp.AUTO,(err,res)=>{
            const data = {index:e.data.index,url:res};
            postMessage(data);
        });
    });
}
