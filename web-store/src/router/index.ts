import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import AuthMiddleware from "@/middleware/AuthMiddleware";
import Login from "@/views/Login.vue";
import BlockView from "@/views/BlockView.vue";
import RegisterBlock from "@/views/RegisterBlock.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/",
    name: "Sites",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Sites.vue"),
    beforeEnter: (to, from, next) => {
      if (to.name !== "Login" && !AuthMiddleware.attempt())
        next({ name: "Login" });
      else next();
    }
  },
  {
    path: "/site/:siteId",
    name: "Site",
    component: () => import("@/views/SiteView.vue"),
    beforeEnter: (to, from, next) => {
      if (to.name !== "Login" && !AuthMiddleware.attempt())
        next({ name: "Login" });
      else next();
    }
  },
  {
    path: "/block/:blockId",
    name: "Block",
    component: BlockView,
    beforeEnter: (to, from, next) => {
      if (to.name !== "Login" && !AuthMiddleware.attempt())
        next({ name: "Login" });
      else next();
    }
  },
  {
    path: "/siteblock/:siteId",
    name: "SiteBlock",
    component: RegisterBlock,
    beforeEnter: (to, from, next) => {
      if (to.name !== "Login" && !AuthMiddleware.attempt())
        next({ name: "Login" });
      else next();
    }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
