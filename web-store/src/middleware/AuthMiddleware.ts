import moment from "moment";
class AuthMiddleware {
  static attempt() {
    const result = localStorage.getItem("x-access-token");
    const expireDate = localStorage.getItem("expires-at");
    if (result) {
      if (!moment().isAfter(expireDate)) {
        return true;
      } else {
        this.logout();
        return false;
      }
    }
    return false;
  }
  static token(): string | null {
    return localStorage.getItem("x-access-token");
  }
  static async login(apiToken: string, expTime: number) {
    localStorage.clear();
    localStorage.setItem("x-access-token", apiToken);
    localStorage.setItem(
      "expires-at",
      moment()
        .add(expTime, "seconds")
        .toISOString()
    );
  }
  static logout() {
    localStorage.clear();
  }
}
export default AuthMiddleware;
