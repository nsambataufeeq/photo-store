import AuthMiddleware from "@/middleware/AuthMiddleware";
import Vue from "vue";
import Component from "vue-class-component";
import Photo from "@/models/Photo";
import { Axios } from "typed-axios";
@Component
class RBlockMixin extends Vue {
  isOwner = false;
  pictures: Array<Photo> = [];
  isOpened = false;
  axios?: Axios;
  getPictures(e: { target: { files: File[] } }) {
    const files = e.target.files;
    const filecount = e.target.files.length;
    const pictures = this.pictures;
    const worker1 = new Worker("/PhotoProcessor.js");
    worker1.onmessage = e => {
      const response: {
        url: string;
        index: number;
      } = e.data;
      const photo = pictures[response.index];
      photo.uri = response.url;
      pictures[response.index] = photo;
      if (response.index == filecount - 1) {
        this.pictures = pictures;
      }
    };
    async function sendMessage(
      id: number,
      fyl: File
    ): Promise<{ index: number; data: ArrayBuffer }> {
      const data = await fyl.arrayBuffer();
      return { index: id, data: data };
    }
    const messages: Promise<{ index: number; data: ArrayBuffer }>[] = [];
    let excessFiles = 0;
    if (files.length > 5) {
      const toastBottom = this.$f7?.toast.create({
        text: "Image count > 3 , truncating...",
        closeTimeout: 2000
      });
      toastBottom?.open();
      excessFiles = files.length - 5;
    }
    try {
      for (let x = 0; x < files.length - excessFiles; x++) {
        messages.push(sendMessage(x, files[x]));
        const photo = new Photo(files[x]);
        pictures.push(photo);
      }
      Promise.allSettled(messages).then(results => {
        results.forEach(result => {
          if (result.status == "fulfilled") {
            worker1.postMessage(
              { index: result.value.index, aBuf: result.value.data },
              [result.value.data]
            );
          }
        });
      });
    } catch (err) {
      alert(err);
    }
  }
  savePhotos(burl: string, sblock = {}) {
    this.isOpened = true;
    const data = { photos: this.pictures, ...sblock };
    if (this.axios) {
      this.axios
        .post(burl, data, {
          headers: {
            Authorization: `Bearer ${AuthMiddleware.token()}`,
            "Content-type": "application/json"
          }
        })
        .then(() => {
          this.isOpened = false;
          this.$router.go(-1);
        })
        .catch(err => {
          if (err.response.status == 401) {
            this.$router.push({ name: "Login" });
          }
          if (err.response.status == 500) {
            this.isOpened = false;
            const toastBottom = this.$f7?.toast.create({
              text: "Unknown Error, Contact Admin",
              closeTimeout: 2000
            });
            toastBottom?.open();
          }
        });
    }
  }
}
export default RBlockMixin;
