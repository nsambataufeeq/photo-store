import AuthMiddleware from "@/middleware/AuthMiddleware";
import axios from "typed-axios";
import Vue from "vue";
import Component from "vue-class-component";
@Component
class SBlockMixin extends Vue {
  isOwner = false;
  owner(burl: string, siteId: number) {
    axios({
      url: burl + `/${siteId}/owner`,
      headers: {
        Authorization: `Bearer ${AuthMiddleware.token()}`
      }
    }).then(response => {
      this.isOwner = response.data;
    });
  }
}
export default SBlockMixin;
