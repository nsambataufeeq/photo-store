import moment from "moment";
class Photo {
  uri = "";
  caption = "";
  name = "";
  mime = "";
  cdate = "";
  constructor(image: File) {
    this.cdate = moment(image.lastModified).format("YYYY-MM-DD");
    this.name = image.name;
    this.mime = image.type;
  }
}
export default Photo;
