import BlockDTO from "@/dtos/BlockDTO";
interface SBDTO extends BlockDTO {
  photos: Array<string>;
  pcount: number;
}

class SiteBlockDTO {
  id: number;
  name: string;
  photo: string;
  pcount: number;
  thumbnails: Array<Array<string>>;
  constructor(sblock: SBDTO) {
    this.id = sblock.id;
    this.name = sblock.name;
    const baseURL = process.env.VUE_APP_BACKEND + process.env.VUE_APP_PHOTOURL;
    const photos = sblock.photos.map(el => {
      const url = baseURL + el;
      return url;
    });
    const photoz = new Array(5);
    photoz.fill(baseURL + "test.jpg");
    photoz.splice(0, photos.length, ...photos);
    this.photo = photoz.shift();
    const thumbnails: Array<Array<string>> = [[]];
    thumbnails.push(photoz.splice(0, 2));
    thumbnails.push(photoz.splice(-2));
    this.thumbnails = thumbnails;
    this.pcount = sblock.pcount;
  }
}
export { SiteBlockDTO, SBDTO };
