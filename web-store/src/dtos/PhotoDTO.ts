interface PDTO {
  url: URL;
  caption: string;
}
class PhotoDTO {
  url: URL;
  caption: string;
  constructor(pdto: PDTO) {
    this.url =
      process.env.VUE_APP_BACKEND + process.env.VUE_APP_PHOTOURL + pdto.url;
    this.caption = pdto.caption;
  }
}
export { PhotoDTO, PDTO };
