interface SDTO {
  id: number;
  name: string;
  lastPhoto: URL;
  elapsedTime: string;
}
class SiteDTO {
  id: number;
  name: string;
  lastPhoto: URL;
  elapsedTime: string;
  constructor(sdto: SDTO) {
    this.id = sdto.id;
    this.name = sdto.name;
    this.lastPhoto =
      process.env.VUE_APP_BACKEND +
      process.env.VUE_APP_PHOTOURL +
      sdto.lastPhoto;
    this.elapsedTime = sdto.elapsedTime;
  }
}
export { SiteDTO, SDTO };
